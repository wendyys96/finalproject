#include "block.h"
#include <cstdlib>
#include "time.h"

Block* set_block (int type){
    switch(type){
    case 1:
        return new I_block;
    case 2:
        return new J_block;
    case 3:
        return new L_block;
    case 4:
        return new O_block;
    case 5:
        return new S_block;
    case 6:
        return new T_block;
    case 7:
        return new Z_block;
    }
    return new Empty_block;
}

Block* random_set(fun_ptr f){
    srand(time(NULL));
    return f(rand() % 8);

}

Block::Block(){}

Block::~Block(){}

void Block::rotate_clockwise(){
    int temp[4][2];
    for (int i = 0; i < 4; ++i){
        temp[i][0] = copy[i][0];
        temp[i][1] = copy[i][0];
    }
    for (int i = 0; i < 4; ++i){
        copy[i][0] = temp[i][1];
        copy[i][1] = -temp[i][0];
    }
}

void Block::rotate_counterclockwise(){
    int temp[4][2];
    for (int i = 0; i < 4; ++i){
        temp[i][0] = copy[i][0];
        temp[i][1] = copy[i][0];
    }
    for (int i = 0; i < 4; ++i){
        copy[i][0] = -temp[i][1];
        copy[i][1] =  temp[i][0];
    }
}

int Block::get_x(int index) const{
    return copy[index][0];
}

int Block::get_y(int index) const{
    return copy[index][1];
}

int Block::get_min_x() const{
    int min = copy[0][0];
    for (int i = 1; i < 4; ++i)
        if(copy[i][0] < min) min = copy[i][0];
    return min;
}

int Block::get_min_y() const{
    int min = copy[0][1];
    for (int i = 1; i < 4; ++i)
        if(copy[i][1] < min) min = copy[i][1];
    return min;
}

int Block::get_max_x() const{
    int max = copy[0][0];
    for (int i = 1; i < 4; ++i)
        if(copy[i][0] > max) max = copy[i][0];
    return max;
}

int Block::get_max_y() const{
    int max = copy[0][1];
    for (int i = 1; i < 4; ++i)
        if(copy[i][1] > max) max = copy[i][1];
    return max;
}

void Block::set_type(Types t){
    type = t;
}

Types Block::get_type() const{
    return type;
}

I_block::I_block(){
    for (int i = 0; i < 4; ++i){
        copy[i][0] = COORD[i][0];
        copy[i][1] = COORD[i][1];
    }
}

J_block::J_block(){
    for (int i = 0; i < 4; ++i){
        copy[i][0] = COORD[i][0];
        copy[i][1] = COORD[i][1];
    }
}

L_block::L_block(){
    for (int i = 0; i < 4; ++i){
        copy[i][0] = COORD[i][0];
        copy[i][1] = COORD[i][1];
    }
}

O_block::O_block(){
    for (int i = 0; i < 4; ++i){
        copy[i][0] = COORD[i][0];
        copy[i][1] = COORD[i][1];
    }
}

S_block::S_block(){
    for (int i = 0; i < 4; ++i){
        copy[i][0] = COORD[i][0];
        copy[i][1] = COORD[i][1];
    }
}

T_block::T_block(){
    for (int i = 0; i < 4; ++i){
        copy[i][0] = COORD[i][0];
        copy[i][1] = COORD[i][1];
    }
}

Z_block::Z_block(){
    for (int i = 0; i < 4; ++i){
        copy[i][0] = COORD[i][0];
        copy[i][1] = COORD[i][1];
    }
}








