#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QLCDNumber>
#include <QLabel>
#include "game.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

private:
    Game* game;
    Ui::MainWindow* ui;
    QLCDNumber* score;
    QPushButton* start;
    QPushButton* pause;
    QLabel* score_label;
    QLabel* next_block;

};

#endif // MAINWINDOW_H
