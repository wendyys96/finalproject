﻿#include "game.h"
#include <iostream>
#include <QKeyEvent>
#include <QColor>
#include <QPainter>
#include <algorithm>

Game::Game(QWidget *parent) : QFrame(parent)
{
   setFrameStyle(QFrame::WinPanel| QFrame::Sunken);
   setFocusPolicy(Qt::StrongFocus); // accepting both keyboard and mouse inputs
   score = 0;
   started = false;
   paused = true;
   try{
      next_block = random_set(set_block);
      if (!next_block) throw std::string("Invalid Block");
   } catch (std::string& error) {
      next_block = new Empty_block;
      std::cerr << error << " Assigned empty block." << std::endl;
   }

}
Game::~Game(){
    if(current_block) delete current_block;
    if(next_block) delete next_block;
}

// when start_button is pressed, the game starts from the beginning
void Game::start(){
    started = true;
    paused = false;
    clear_board();
    set_piece();
    timer.start(1000, this); // the timer will stop after 1000 msec.
}

// when pause_button is pressed, the game will be paused. If pressed again, the game will start.
void Game::pause(){
    if(!started) return;
    paused = !paused;
    if(!paused) timer.stop();
    else timer.start(1000, this);
}



//the following keyboard inputs call specific functions
void Game::keyPressEvent(QKeyEvent *event){
    if(!started || paused || current_block->get_type() == empty_block){
        QFrame::keyPressEvent(event);
        return;
    }
    switch(event->key()){
       case Qt::Key_Left:
           move_block(current_block, cur_x - 1, cur_y);
       case Qt::Key_Right:
           move_block(current_block, cur_x + 1, cur_y);
       case Qt::Key_Down:
           move_block(current_block, cur_x, cur_y - 1);
       case Qt::Key_A:
           current_block->rotate_clockwise();
           update();
       case Qt::Key_S:
           current_block->rotate_counterclockwise();
           update();
    }
    QFrame::keyPressEvent(event);
}

void Game::set_piece(){
    if(current_block) delete current_block;
    current_block = next_block;
    try{
        next_block = random_set(set_block);
        if(!next_block) throw std::invalid_argument("Warning: Invalid block! \n");
    } catch (std::invalid_argument) {
        next_block = new Empty_block;
    }
    cur_x = BOARD_WIDTH / 2;
    cur_y = BOARD_HEIGHT - 1;
    try {
        if (move_block(current_block, cur_x, cur_y)) throw (std::string("Invalid location"));
    } catch(std::string err){
        current_block->set_type(empty_block);
        std::cerr << err << std::endl;
    }


}

bool Game::move_block(Block* new_block, int new_x, int new_y){
    for(int i = 0; i < 4; ++i){
        int x = new_block->get_x(i) + new_x;
        int y = new_block->get_y(i) + new_y;
        if(x < 0 || x >= BOARD_WIDTH || y < 0 || y >= BOARD_HEIGHT)
            return false;
        if(get_shape_at(x, y) != empty_block)
            return false;
    }
    delete current_block;
    current_block = new_block;
    cur_x = new_x;
    cur_y = new_y;
    update();
    return true;
}


//Drawing the squares of the block with the choice of color
void Game::draw_block(QPainter& painter, int x, int y, Types choice){
    QColor colors[8] = {QColor(Qt::gray), QColor(Qt::blue), QColor(Qt::green), QColor(Qt::yellow), QColor(Qt::cyan), QColor(Qt::magenta), QColor(Qt::red), QColor(Qt::black)};
    QColor color = colors[choice];
    painter.setBrush(color);
    painter.drawRect(x, y, square_width(), square_height());
}

//Draw the tetris block
void Game::paintEvent(QPaintEvent* event){
    QFrame::paintEvent(event);
    QPainter painter(this);
    for(int i = 0; i < BOARD_HEIGHT; ++i){
        for(int j = 0; j < BOARD_WIDTH; ++j){
            if(get_shape_at(j, BOARD_HEIGHT - i - 1) != empty_block){
                Types type = get_shape_at(j, BOARD_HEIGHT - i - 1);
                draw_block(painter, contentsRect().left() + j * square_width(), i * square_height(), type);
            }
        }
     }
    for(int i = 0; i < 4; ++i){
        int x = current_block->get_x(i) + cur_x;
        int y = current_block->get_y(i) + cur_y;
        draw_block(painter, contentsRect().left() + x * square_width(), y * square_height(), current_block->get_type());
    }
}

//when the block is dropped as far down as possible, the board is assigned the block type since its position is now permanent.
void Game::dropped_bottom(){
    for(int i = 0; i < 4; ++i){
        int x = current_block->get_x(i) + cur_x;
        int y = current_block->get_y(i) + cur_y;
        set_shape_at(x, y, current_block->get_type());
    }
}


void Game::remove_line(){
    int line_to_remove = 0;
    bool line_full = true;
    for(int i = 0; i < BOARD_HEIGHT; ++i){
        for(int j = 0; j < BOARD_WIDTH; ++j){
            if(get_shape_at(j, BOARD_HEIGHT - i -1) == empty_block) {
                line_full = false;
                break;
            }
        }
        if(line_full) ++line_to_remove;
    }
    if(line_to_remove == 0) return;
    for(int i = 0; i < line_to_remove; ++i){
        for(int j = 0; j < BOARD_WIDTH; ++j){
            set_shape_at(j, BOARD_HEIGHT - i - i, empty_block);
        }
    }
    update();
}


Types Game::get_shape_at(int x, int y){
    return board[BOARD_WIDTH * y + x];
}

void Game::set_shape_at(int x, int y, Types type){
    board[BOARD_WIDTH * y + x] = type;
}

int Game::square_height()const{
    return contentsRect().height() / BOARD_WIDTH;
}

int Game::square_width()const{
    return contentsRect().width() / BOARD_WIDTH;
}

void Game::clear_board(){
    std::for_each(board, board + 200, [](Types& t){t = empty_block;});
}















