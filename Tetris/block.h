#ifndef BLOCK_H
#define BLOCK_H

enum Types {
    empty_block, i_block, j_block, l_block, o_block, s_block, t_block, z_block
};


class Block{
public:
    virtual ~Block() = 0;
    void rotate_clockwise();
    void rotate_counterclockwise();
    int get_x(int) const;
    int get_y(int) const;
    int get_min_x() const;
    int get_min_y() const;
    int get_max_x() const;
    int get_max_y() const;
    void set_type(Types);
    Types get_type() const;
protected:
    Block();
    Types type = empty_block;
    int copy[4][2] = {{0, 0}, {0, 0}, {0, 0}, {0, 0}};

};

class Empty_block :public Block{
public:
    Empty_block(){}
    ~Empty_block(){}
};


class I_block :public Block{
public:
    I_block();
    ~I_block(){}
private:
    const int COORD[4][2] = {{-1, 0}, {0, 0}, {1, 0}, {2, 0}};
};

class J_block :public Block{
public:
    J_block();
    ~J_block(){}
private:
    const int COORD[4][2] = {{0, 1}, {0, 0}, {1, 0}, {2, 0}};
};

class L_block :public Block{
public:
    L_block();
    ~L_block(){}
private:
    const int COORD[4][2] = {{0, 0}, {1, 0}, {2, 0}, {2, 1}};
};

class O_block :public Block{
public:
    O_block();
    ~O_block(){}
private:
    const int COORD[4][2] = {{0, 0}, {0, 1}, {1, 0}, {1, 1}};
};

class S_block :public Block{
public:
    S_block();
    ~S_block(){}
private:
    const int COORD[4][2] = {{-1, 0}, {0, 0}, {0, 1}, {1, 1}};
};

class T_block :public Block{
public:
    T_block();
    ~T_block(){}
private:
    const int COORD[4][2] = {{-1, 0}, {0, 0}, {0, 1}, {1, 0}};
};

class Z_block :public Block{
public:
    Z_block();
    ~Z_block(){}
private:
    const int COORD[4][2] = {{-1, 1}, {0, 0}, {0, 1}, {1, 0}};
};


Block* set_block(int);

typedef Block* (*fun_ptr) (int);

Block* random_set(fun_ptr);

#endif // BLOCK_H
