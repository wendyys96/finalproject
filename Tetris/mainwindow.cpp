#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGridLayout>

MainWindow::MainWindow(QWidget* parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QWidget* ui_area = new QWidget;
    setCentralWidget(ui_area);
    game = new Game;
    score = new QLCDNumber;
    start = new QPushButton("Start");
    pause = new QPushButton("Pause");
    next_block = new QLabel("Next");
    score_label = new QLabel("Score");

    connect(start, SIGNAL(clicked(bool)), game, SLOT(start()));
    connect(pause, SIGNAL(clicked(bool)), game, SLOT(pause()));
    connect(game, SIGNAL(score_changed(int)), score, SLOT(display(int)));

    QGridLayout* layout = new QGridLayout;
    layout->addWidget(game, 0, 1, 6, 1);
    layout->addWidget(next_block, 0, 0);
    layout->addWidget(score_label, 0, 2);
    layout->addWidget(score, 1, 2);
    layout->addWidget(start, 3, 0);
    layout->addWidget(pause, 3, 2);

    ui_area->setLayout(layout);
    setWindowTitle("Tetris");



}

MainWindow::~MainWindow()
{
    delete ui;

}
