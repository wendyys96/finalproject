#ifndef GAME_H
#define GAME_H

#include <QFrame>
#include <QBasicTimer>
#include <QWidget>
#include "block.h"


class Game :public QFrame
{
    Q_OBJECT
public:
 Game(QWidget* parent = 0);
 ~Game();
public slots:
 void start();
 void pause();
 signals:
 void score_changed(int);


private:
 static const int BOARD_WIDTH = 10;
 static const int BOARD_HEIGHT = 20;
 Types board[BOARD_HEIGHT * BOARD_WIDTH];
 int cur_x = 0;
 int cur_y = 0;
 int square_height() const;
 int square_width() const;
 int score;
 bool started;
 bool paused;
 QBasicTimer timer;
 Block* current_block = nullptr;
 Block* next_block;
 void keyPressEvent(QKeyEvent*);
 void paintEvent(QPaintEvent*);
 bool move_block(Block*, int, int);
 void set_piece();
 void draw_block(QPainter&, int, int, Types);
 void dropped_bottom();
 Types get_shape_at(int, int);
 void set_shape_at(int, int, Types);
 void clear_board();
 void remove_line();
};

#endif // GAME_H
