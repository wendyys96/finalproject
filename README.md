#Final Project
## Tetris Game

I used Qt and C++ to write a code for a Tetris game. 
I also tried to apply some of the topics we learned in class:  
  
  **Inheritance/Polymorphism:**  
  Block is a base class which other block types such as the I_block inherits publicly from.  
  I used the base pointer to represent the derived classes on the heap.  
    
  Block is an abstract class by having the destructor to be purely virtual.  
  Its constructor is protected to emphasize that it cannot be instantiated as an object.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Block{
public:
    virtual ~Block() = 0;
 
protected:
    Block();

};

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  
  **Memory Managemet:**  
  I used a virtual destructor for the Block class.  
  When using a base class pointer to point to derived class objects, we need to be careful with memory deallocation.  
  Without virtual destuctors, the base class destructor will be called and will not release the memory only held by the derived class.  
  
  I also used try & catch.  
  Whenever I had the block pointer assigned a derived class object on the heap, I made sure that the pointer is not pointing to null.  
  The user is notified of the error, and an empty block is assigned to the pointer. This way, the program will continue to run.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
try{
       next_block = Random_set()(Set_block());
       if (!next_block) throw std::string("Invalid Block");
   } catch (std::string& error) {
      next_block = new Empty_block();
      std::cerr << error << " Assigned empty block." << std::endl;
   }
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  I tried using templates and functors  
  
  **Functors:**  
  Random_set and Set_block are classes but with the operator() act like a function. Bottom is the defintion of Set_block.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Set_block{
    Set_block(){}
    ~Set_block(){}
    Block* operator()(int);
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   
  
  **Templates:**  
  When implementing the functor Random_set I used a template to receive another functor, Set_block as a parameter.
  
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template<typename Unary_fun>
class Random_set{
    Random_set(){}
    ~Random_set(){}
    Block* operator()(Unary_fun);
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  
  
  Instead, I used function pointets in the end.  
  **Function Pointers:**  
  Although not necessary in this one because the function pointer will not be used many times,  
  I used typedef to have fun_ptr refer to a pointer to a function returning a Block* and receiving an int as a parameter.  
  This is especially useful when we are using multiple function pointers of the same type.  
  We do not have to write out the long declaration every time we declare a pointer but just once. 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
Block* set_block(int);

typedef Block* (*fun_ptr) (int);

Block* random_set(fun_ptr);

Block* random_set(fun_ptr f){
    srand(time(NULL));
    return f(rand() % 8);

}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  
  **Algorithm and Lambda Functions:**  
  I used the for_each function in the algorithm library.  
  With the lambda function receiving Types& as input and assigning the input as empty_block.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
#include <algorithm>
void Game::clear_board(){
    std::for_each(board, board + 200, [](Types& t){t = empty_block;});
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
  
  
  